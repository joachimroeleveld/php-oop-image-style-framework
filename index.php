<?php 
	require 'image_styles.php';

	$file = 'images/image.jpg';
	$destination = 'images/image_processed.jpg';

	$example_style->generate($file, $destination);
?>

<html>
<head>
	<title>Example</title>
</head>
<body>
	<h1>Example</h1>
	<p>
		<h3>Unprocessed</h3>
		<img src="<?php print($file); ?>" />
	</p>

	<p>
		<h3>Processed</h3>
		<img src="<?php print($destination); ?>">
	</p>
</body>
</html>