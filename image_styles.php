<?php
    require_once 'ImageStyle.php';
    require_once 'Operation.php';

    // OPERATIONS

    $resize_and_crop = new Operation(
        'resize_crop',
        array('width', 'height'),
        'resize_image'
    );

    // IMAGE STYLES

    $example_style = new ImageStyle(
        'example_style',
        array($resize_and_crop),
        array(
            'resize_crop' => array(
                'width' => 90,
                'height' => 90
            )
        )
    );