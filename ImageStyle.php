<?php
	class ImageStyle {
		private $name;
		private $operations = array();
		private $values = array();

		function __construct($name, $operations = array(), $values = array()) {
			$this->name = $name;
			$this->operations = $operations;
			$this->values = $values;
		}

		function addOperation($operation, $values = array()) {
			$this->operations[] = $operation;
			$this->values[$operation->getName()] = $values;
		}

		function setValues($operation, $values) {
			$this->values[$operation->getName()] = $values;
		}

		/**
		* With this function, an image can be actually processed by each 
		* operation (defined in $operations)
		*/
		function generate($file, $destination) {
			for ($i = 0; $i < count($this->operations); $i++) { 
				$operation = $this->operations[$i];
				if ($i == 1)
					$file = $destination;

				$values = $this->values[$operation->getName()];
				$operation->generate($values, $file, $destination);
			}
		}

		function getName() {
			return $this->name;	
		}

		function getOperations() {
			return $this->operations;	
		}

		function getValues() {
			return $this->values;
		}
	}