<?php
	require_once 'operations.php';


	class Operation {
		private $name;
		// Properties are only for review; the values for these are declared
		// in an image style.
		private $properties = array();
		private $process_function;

		function __construct($name, $properties = array(), 
			$process_function = NULL) 
		{
			$this->name = $name;
			$this->properties = $properties;
			$this->process_function = $process_function;
		}

		function addProperty($property) {
			$this->properties[] = $property;
		}

		function setFunction($process_function) {
			$this->process_function = $process_function;
		}

		/**
		* This function is called when calling generate() on an image style. 
		* When this is executed, the process function for this operation 
		* will be called, passing the information along with it.
		*/
		function generate($values, $file, $destination) {
			if (isset($this->process_function)) 
		{
				call_user_func_array($this->process_function, 
					array($values, $file, $destination));
			}
		}

		function getName() {
			return $this->name;	
		}

		function getProperties() {
			return $this->properties;	
		}

		function getFunction() {
			return $this->process_function;
		}
	}